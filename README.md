# Halloween UFO version 2

The [first UFO](http://retrojdm.com/ArduinoNeoPixelUFO.asp) got destroyed by racoons when it was in storage at the family farm.

This second version is smaller, for easier storage.

[![Halloween UFO version 2](./ufo2.jpg)](https://www.youtube.com/watch?v=8RC7Fsu0h7w)

See a [YouTube video](https://www.youtube.com/watch?v=8RC7Fsu0h7w) of it here.
